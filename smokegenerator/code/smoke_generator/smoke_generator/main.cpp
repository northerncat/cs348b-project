#include <iostream>
#include <math.h>
#include <vector>
#include <string>
#include <stdlib.h>
#include <algorithm>
#include <fstream>
#include <iomanip>

using namespace std;

// SIZE OF GRID
int xdim = 50;
int ydim = 50;
int zdim = 50;
int N = xdim * ydim * zdim;


// inline index function for storing 3D matrix in 1D array 
inline int idx(int x, int y, int z)
{
	return ((y + z * (ydim + 2)) * (xdim + 2) + x);
}


void setBoundary(int flag, float * d)
{
	for (int x = 1; x <= xdim; x++) {
		d[idx(x, 0, 0)]					= flag == 1 ? -d[idx(x, 1, 1)]		 : d[idx(x, 1, 1)];
		d[idx(x, ydim + 1, 0)]			= flag == 1 ? -d[idx(x, ydim, 1)]	 : d[idx(x, ydim, 1)];
		d[idx(x, 0, zdim + 1)]			= flag == 1 ? -d[idx(x, 1, zdim)]	 : d[idx(x, 1, zdim)];
		d[idx(x, ydim + 1, zdim + 1)]	= flag == 1 ? -d[idx(x, ydim, zdim)] : d[idx(x, ydim, zdim)];
	}

	for (int y = 1; y <= ydim; y++) {
		d[idx(0, y, 0)]					= flag == 2 ? -d[idx(1, y, 1)]		 : d[idx(1, y, 1)];
		d[idx(xdim + 1, y, 0)]			= flag == 2 ? -d[idx(xdim, y, 1)]	 : d[idx(xdim, y, 1)];
		d[idx(0, y, zdim + 1)]			= flag == 2 ? -d[idx(1, y, zdim)]	 : d[idx(0, y, zdim)];
		d[idx(xdim + 1, y, zdim + 1)]	= flag == 2 ? -d[idx(xdim, y, zdim)] : d[idx(xdim, y, zdim)];
	}

	for (int z = 1; z <= zdim; z++) {
		d[idx(0, 0, z)]					= flag == 3 ? -d[idx(1, 1, z)]		 : d[idx(1, 1, z)];
		d[idx(xdim + 1, 0, z)]			= flag == 3 ? -d[idx(xdim, 1, z)]	 : d[idx(xdim, 1, z)];
		d[idx(0, ydim + 1, z)]			= flag == 3 ? -d[idx(1, ydim, z)]	 : d[idx(0, ydim, z)];
		d[idx(xdim + 1, ydim + 1, z)]	= flag == 3 ? -d[idx(xdim, ydim, z)] : d[idx(xdim, ydim, z)];
	}

	d[idx(0, 0, 0)] = (1.f / 3.f) * (d[idx(1, 0, 0)] + d[idx(0, 1, 0)] + d[idx(0, 0, 1)]);
	d[idx(xdim + 1, 0, 0)] = (1.f / 3.f) * (d[idx(xdim, 0, 0)] + d[idx(xdim + 1, 1, 0)] + d[idx(xdim + 1, 0, 1)]);
	d[idx(0, ydim + 1, 0)] = (1.f / 3.f) * (d[idx(0, ydim, 0)] + d[idx(1, ydim + 1, 0)] + d[idx(0, ydim + 1, 1)]);
	d[idx(0, 0, zdim + 1)] = (1.f / 3.f) * (d[idx(0, 0, zdim)] + d[idx(1, 0, zdim + 1)] + d[idx(0, 1, zdim + 1)]);
	d[idx(xdim + 1, ydim + 1, 0)] = (1.f / 3.f) * (d[idx(xdim, ydim + 1, 0)] + d[idx(xdim + 1, ydim, 0)] + d[idx(xdim + 1, ydim + 1, 1)]);
	d[idx(xdim + 1, 0, zdim + 1)] = (1.f / 3.f) * (d[idx(xdim, 0, zdim + 1)] + d[idx(xdim + 1, 0, zdim)] + d[idx(xdim + 1, 1, zdim + 1)]);
	d[idx(0, ydim + 1, zdim + 1)] = (1.f / 3.f) * (d[idx(0, ydim, zdim + 1)] + d[idx(0, ydim + 1, zdim)] + d[idx(1, ydim + 1, zdim + 1)]);
	d[idx(xdim + 1, ydim + 1, zdim + 1)] = (1.f / 3.f) * (d[idx(xdim, ydim + 1, zdim + 1)] + d[idx(xdim + 1, ydim, zdim + 1)] + d[idx(xdim + 1, ydim + 1, zdim)]);
}


// compute diffusion
void diffuse(float * d, float * d0, float diff, float dt, int numIters = 20)
{
	float a = dt * diff * N;

	// Gauss-Seidel relaxation iterative solver
	for (int itr = 0; itr<numIters; itr++)
	{
		for (int x = 1; x <= xdim; x++) {
			for (int y = 1; y <= ydim; y++) {
				for (int z = 1; z <= zdim; z++) {

					d[idx(x, y, z)] = (d0[idx(x, y, z)] + a * (d[idx(x - 1, y, z)] + d[idx(x + 1, y, z)] +
															   d[idx(x, y - 1, z)] + d[idx(x, y + 1, z)] +
															   d[idx(x, y, z - 1)] + d[idx(x, y, z + 1)])) / (1 + 6.f * a);

				}
			}
		}
	}
}

// perform linear backtrace and interpolation
void advect(float * d, float * d0, float * u, float * v, float * w, float dt)
{
	float dt0x = dt * xdim; // ?
	float dt0y = dt * ydim; // ?
	float dt0z = dt * zdim; // ?

	for (int i = 1; i <= xdim; i++) {
		for (int j = 1; j <= ydim; j++) {
			for (int k = 1; k <= zdim; k++) {

				// step backwards
				int x = i - dt0x * u[idx(i, j, k)];
				int y = j - dt0y * v[idx(i, j, k)];
				int z = k - dt0z * w[idx(i, j, k)];

				// get indices of nearest points
				if (x < 0.5)			x = 0.5;
				if (x > xdim + 0.5)		x = xdim + 0.5;
				int i0 = (int)x;
				int i1 = i0 + 1;

				if (y < 0.5)			y = 0.5;
				if (y > ydim + 0.5)		y = ydim + 0.5;
				int j0 = (int)y;
				int j1 = j0 + 1;

				if (z < 0.5)			z = 0.5;
				if (z > zdim + 0.5)		z = zdim + 0.5;
				int k0 = (int)z;
				int k1 = k0 + 1;

				// calculate interoplation weightings
				float s1 = x - i0;
				float s0 = 1 - s1;

				float t1 = y - j0;
				float t0 = 1 - t1;

				float r1 = z - k0;
				float r0 = 1 - r1;

				// interpolate in 3D
				d[idx(i, j, k)] = r0 * (s1 * (t0 * d0[idx(i0, j0, k0)] + t1 * d0[idx(i0, j1, k0)]) +
										s0 * (t0 * d0[idx(i1, j0, k0)] + t1 * d0[idx(i1, j1, k0)])) +
								  r1 * (s1 * (t0 * d0[idx(i0, j0, k1)] + t1 * d0[idx(i0, j1, k1)]) +
										s0 * (t0 * d0[idx(i1, j0, k1)] + t1 * d0[idx(i1, j1, k1)]));

			}
		}
	}

}

// solve poisson equation
void project(float * u, float * v, float * w, float * p, float * div, int numIters = 20)
{
	float hx = 1.0f / xdim; // ?
	float hy = 1.0f / ydim;
	float hz = 1.0f / zdim;

	for (int x = 1; x <= xdim; x++) {
		for (int y = 1; y <= ydim; y++) {
			for (int z = 1; z <= zdim; z++) {

				div[idx(x, y, z)] = - 0.5f * ( hx * (u[idx(x + 1, y, z)] - u[idx(x - 1, y, z)]) +
											   hy * (v[idx(x, y + 1, z)] - v[idx(x, y - 1, z)]) +
											   hz * (w[idx(x, y, z + 1)] - w[idx(x, y, z - 1)]));

				p[idx(x, y, z)] = 0;

			}
		}
	}

	// Gauss-Seidel relaxation iterative solver
	for (int itr = 0; itr<numIters; itr++)
	{
		for (int x = 1; x <= xdim; x++) {
			for (int y = 1; y <= ydim; y++) {
				for (int z = 1; z <= zdim; z++) {

					p[idx(x, y, z)] = (div[idx(x, y, z)] +  p[idx(x - 1, y, z)] + p[idx(x + 1, y, z)] +
														    p[idx(x, y - 1, z)] + p[idx(x, y + 1, z)] +
															p[idx(x, y, z - 1)] + p[idx(x, y, z + 1)]) / 6.f;

				}
			}
		}
	}

	for (int x = 1; x <= xdim; x++) {
		for (int y = 1; y <= ydim; y++) {
			for (int z = 1; z <= zdim; z++) {

				u[idx(x, y, z)] -= 0.5 * (p[idx(x + 1, y, z)] - p[idx(x - 1, y, z)]) / hx;
				v[idx(x, y, z)] -= 0.5 * (p[idx(x, y + 1, z)] - p[idx(x, y - 1, z)]) / hy;
				w[idx(x, y, z)] -= 0.5 * (p[idx(x, y, z + 1)] - p[idx(x, y, z - 1)]) / hz;

			}
		}
	}

}

// return l2-norm of vector 
float norm(float * a, int n = 3)
{
	float sum = 0;
	for (int i = 0; i < n; i++)
		sum += a[i] * a[i];

	return sqrt(sum);
}

void vorticityConfinement(int size, float * fu, float * fv, float * fw, const float * u0, const float * v0, const float * w0, float dt)
{
	float hx = 1.f;
	float hy = ydim / (float)xdim;
	float hz = zdim / (float)xdim;

	float eps = 60.f;

	float * w1 = (float*)malloc(size * sizeof(float));
	float * w2 = (float*)malloc(size * sizeof(float));
	float * w3 = (float*)malloc(size * sizeof(float));

	if (w1 == NULL || w2 == NULL || w3 == NULL)
		cerr << "Faled to allocate omega matrices" << endl;

	memset(w1, 0, sizeof(w1));
	memset(w2, 0, sizeof(w2));
	memset(w3, 0, sizeof(w3));
	
	for (int x = 1; x <= xdim; x++) {
		for (int y = 1; y <= ydim; y++) {
			for (int z = 1; z <= zdim; z++) {

				w1[idx(x, y, z)] = (w0[idx(x, y + 1, z)] - w0[idx(x, y - 1, z)] - v0[idx(x, y, z + 1)] + v0[idx(x, y, z - 1)]) / (2.f * hx);
				w2[idx(x, y, z)] = (u0[idx(x, y, z + 1)] - u0[idx(x, y, z - 1)] - w0[idx(x + 1, y, z)] + w0[idx(x - 1, y, z)]) / (2.f * hy);
				w3[idx(x, y, z)] = (v0[idx(x + 1, y, z)] - v0[idx(x - 1, y, z)] - u0[idx(x, y + 1, z)] + u0[idx(x, y - 1, z)]) / (2.f * hz);

			}
		}
	}

	for (int x = 1; x <= xdim; x++) {
		for (int y = 1; y <= ydim; y++) {
			for (int z = 1; z <= zdim; z++) {

				float gradMagW[3] = { 0, 0, 0 };

				float x1[3] = { w1[idx(x + 1, y, z)], w2[idx(x + 1, y, z)], w3[idx(x + 1, y, z)] };
				float x2[3] = { w1[idx(x - 1, y, z)], w2[idx(x - 1, y, z)], w3[idx(x - 1, y, z)] };

				gradMagW[0] = (norm(x1) - norm(x2)) / 2.f;

				float y1[3] = { w1[idx(x, y + 1, z)], w2[idx(x, y + 1, z)], w3[idx(x, y + 1, z)] };
				float y2[3] = { w1[idx(x, y - 1, z)], w2[idx(x, y - 1, z)], w3[idx(x, y - 1, z)] };

				gradMagW[1] = (norm(y1) - norm(y2)) / 2.f;

				float z1[3] = { w1[idx(x, y, z + 1)], w2[idx(x, y, z + 1)], w3[idx(x, y, z + 1)] };
				float z2[3] = { w1[idx(x, y, z - 1)], w2[idx(x, y, z - 1)], w3[idx(x, y, z - 1)] };

				gradMagW[2] = (norm(z1) - norm(z2)) / 2.f;

				float normFactor = norm(gradMagW);
				if (normFactor == 0)	normFactor = 1;	// if norm is zero, all terms are zero so this doesn't matter

				float eta[3] = { gradMagW[0] / normFactor, gradMagW[1] / normFactor, gradMagW[2] / normFactor };

				float v[3] = { w1[idx(x, y, z)], w2[idx(x, y, z)], w3[idx(x, y, z)] };

				fu[idx(x, y, z)] = (eta[1] * v[2] - eta[2] * v[1]) * eps * hx;
				fv[idx(x, y, z)] = (eta[2] * v[0] - eta[0] * v[2]) * eps * hy;
				fw[idx(x, y, z)] = (eta[0] * v[1] - eta[1] * v[0]) * eps * hz;

			}
		}
	}

	free(w1);
	free(w2);
	free(w3);

}


void addVorticityForce(int size, float * d, float * f, float dt)
{
	for (int i = 0; i < size; i++)
		d[i] += f[i] * dt;
}


// w[] == z-matrix
// p[] == density matrix
// T[] == temperature matrix
void addBuoyancyForce(int size, float * w, float * p, float * T, float ambientTemp, float dt, float alpha = 1.0, float beta = 0.1)
{
	for (int i = 0; i < size; i++)
		w[i] += dt * (-alpha * p[i] + beta * (T[i] - ambientTemp));
}



template<typename T>
double normDiff(T * a, T * b, int n = 3)
{
	T res = 0;
	for (int i = 0; i < n; i++)
		res += (a[i] - b[i]) * (a[i] - b[i]);

	return sqrt(res);
}


void initTemperature(int size, float * T, float ambientTemp)
{
	for (int i = 0; i < size; i++)
		T[i] = ambientTemp;
}

float sampleGaussian(float mu, float sigma, float x)
{
	return (1.0 / sqrt(2 * sigma * sigma * 3.14159)) * exp(-(x - mu) * (x - mu) / (2 * sigma * sigma));
}

void insertSource(float * dens, float * T, float ambientTemp, float radius = 2.f, int height = 1, int depth = 2, float vel = 0.1f)
{
	height += 1;
	for (int z = height; z < height+depth && z <= zdim; z++)
	{
		float srcCenter[3] = { (xdim + 2 - 1) / 2.f, (ydim + 2 - 1) / 2.f, z };

		for (int x = 1; x <= xdim; x++) {
			for (int y = 1; y <= ydim; y++) {

				float currentPt[3] = { x, y, z };
				float nDiff = normDiff(srcCenter, currentPt);

				if (nDiff <= radius) {
					
					// set densities to 1.0
					dens[idx(x, y, z)] = 1.0f;

					// set temperatures between 2*ambient and 4*ambient based on Gaussian dist
					T[idx(x, y, z)] = ambientTemp * (2 + 2 * sampleGaussian(0, radius / 3.f, nDiff / radius));
					//T[idx(x, y, z)] = ambientTemp * (1 + 5 * sampleGaussian(0, radius / 3.f, nDiff / radius));

					//w[idx(x, y, z)] = vel;
					//u[idx(x, y, z)] = 2 * ((double)rand() / (double)RAND_MAX) - 1;
					//v[idx(x, y, z)] = 2 * ((double)rand() / (double)RAND_MAX) - 1;
				}

			}
		}
	}
}


void swap_ptrs(float ** a, float ** b)
{
	float * temp = *a;
	*a = *b;
	*b = temp;
}

void densityStep(float ** d, float ** d0, float * u, float * v, float * w, float diff, float dt, int solverIters = 40)
{
	diffuse(*d, *d0, diff, dt, solverIters);	// setBoundary(0, *d);
	advect(*d0, *d, u, v, w, dt);				// setBoundary(0, *d0);
	swap_ptrs(d, d0);
}

void temparatureStep(float ** T, float ** T0, float * u, float * v, float * w, float heat, float dt, int solverIters = 40)
{
	diffuse(*T, *T0, heat, dt, solverIters);	
	advect(*T0, *T, u, v, w, dt);				
	swap_ptrs(T, T0);
}

void velocityStep(float ** u, float ** v, float ** w, float ** u0, float ** v0, float ** w0, float visc, float dt, int solverIters = 40)
{
	diffuse(*u, *u0, visc, dt, solverIters);	// setBoundary(1, *u);
	diffuse(*v, *v0, visc, dt, solverIters);	// setBoundary(2, *v);
	diffuse(*w, *w0, visc, dt, solverIters);	// setBoundary(3, *w);

	project(*u, *v, *w, *u0, *v0, solverIters);
	swap_ptrs(u, u0);
	swap_ptrs(v, v0);
	swap_ptrs(w, w0);

	advect(*u, *u0, *u0, *v0, *w0, dt);		// setBoundary(1, *u);
	advect(*v, *v0, *u0, *v0, *w0, dt);		// setBoundary(2, *v);
	advect(*w, *w0, *u0, *v0, *w0, dt);		// setBoundary(3, *w);

	project(*u, *v, *w, *u0, *v0, solverIters);
}


void writeDensityGrid(float * d, float * p0, float * p1)
{
	ofstream file;
	file.open("density_grid.pbrt");

	file << "Volume \"volumegrid\" \"integer nx\" " << xdim << " ";
	file << "\"integer ny\" " << ydim << " ";
	file << "\"integer nz\" " << zdim << " \n";

	file << fixed << setprecision(6);

	file << "\t\"point p0\" [ " << p0[0] << " " << p0[1] << " " << p0[2] << " ] ";
	file << "\"point p1\" [ " << p1[0] << " " << p1[1] << " " << p1[2] << " ]\n";
	file << "\t\"float density\" [\n";

	file << setprecision(3);

	for (int y = 1; y <= ydim; y++) {
		for (int z = 1; z <= zdim; z++) {
			for (int x = 1; x <= xdim; x++) {
				file << d[idx(x, y, z)] << " ";
				if (x == xdim)
					file << "\n";
			}
		}
	}

	file << "]\n";

	file.close();
}



// *
// * MAIN FUNCTION : runs smoke simulation
// *
int main(int argc, char **argv)
{
	// intialize vars
	float dt = 0.001f;
	float diff = 0.05f;
	float visc = 0.01f;
	float heat = 0.01f;

	float gravForce = 1.0;
	float tempForce = 0.1;

	int srcHeight = 4;
	int srcDepth = 4;
	float srcRadius = 2.f;
	float srcVel = dt;

	int simIters = 20;
	int solverIters = 40;

	float bbox0[3] = { 0,0,0 };
	float bbox1[3] = { 1,1,1 };

	float ambientTemp = 26.f;

	// read parameters from .ini file
	static const char * input_file = "params.ini";

	std::fstream f(input_file, std::ios::in);

	if (!f.is_open()) {
		std::cout << "File not found: " << input_file << std::endl;
		return 1;
	}

	while (f.good()) {
		std::string line;
		std::getline(f, line);

		if ((line.substr(0, 1) == "#") || line.empty()) {
			continue;
		}

		std::string::size_type offset = line.find_first_of('=');
		std::string key = line.substr(0, offset - 1);
		std::string value = line.substr(offset + 2, line.length() - (offset + 2));

		if (key == "xdim")			xdim = atoi(value.c_str());
		if (key == "ydim")			ydim = atoi(value.c_str());
		if (key == "zdim")			zdim = atoi(value.c_str());
		if (key == "srcHeight")		srcHeight = atoi(value.c_str());
		if (key == "srcDepth")		srcDepth = atoi(value.c_str());
		if (key == "srcRadius") 	srcRadius = atof(value.c_str());
		if (key == "srcVelocity")	srcVel = atof(value.c_str());
		if (key == "dt") 			dt = atof(value.c_str());
		if (key == "diff") 			diff = atof(value.c_str());
		if (key == "visc")			visc = atof(value.c_str());
		if (key == "heat")			heat = atof(value.c_str());
		if (key == "gravForce")		gravForce = atof(value.c_str());
		if (key == "tempForce")		tempForce = atof(value.c_str());
		if (key == "simIters")		simIters = atoi(value.c_str());
		if (key == "solverIters")	solverIters = atoi(value.c_str());
		if (key == "bboxX0")		bbox0[0] = atof(value.c_str());
		if (key == "bboxY0")		bbox0[1] = atof(value.c_str());
		if (key == "bboxZ0")		bbox0[2] = atof(value.c_str());
		if (key == "bboxX1")		bbox1[0] = atof(value.c_str());
		if (key == "bboxY1")		bbox1[1] = atof(value.c_str());
		if (key == "bboxZ1")		bbox1[2] = atof(value.c_str());

	}

	f.close();

	// get total grid size with borders
	const int size = (xdim + 2) * (ydim + 2) * (zdim + 2);
	N = xdim * ydim * zdim;

	// allocate grids	
	float * dens = (float*)malloc(size * sizeof(float));
	float * dens_prev = (float*)malloc(size * sizeof(float));

	float * temp = (float*)malloc(size * sizeof(float));
	float * temp_prev = (float*)malloc(size * sizeof(float));

	float * u = (float*)malloc(size * sizeof(float));
	float * v = (float*)malloc(size * sizeof(float));
	float * w = (float*)malloc(size * sizeof(float));

	float * u_prev = (float*)malloc(size * sizeof(float));
	float * v_prev = (float*)malloc(size * sizeof(float));
	float * w_prev = (float*)malloc(size * sizeof(float));

	// set everything to zeros
	memset(dens, 0, sizeof(dens));
	memset(dens_prev, 0, sizeof(dens_prev));
	memset(temp, 0, sizeof(temp));
	memset(temp_prev, 0, sizeof(temp_prev));
	memset(u, 0, sizeof(u));
	memset(v, 0, sizeof(v));
	memset(w, 0, sizeof(w));
	memset(u_prev, 0, sizeof(u_prev));
	memset(v_prev, 0, sizeof(v_prev));
	memset(w_prev, 0, sizeof(w_prev));

	// run simulation
	cout << "Running simulation..." << endl;
	initTemperature(size, temp, ambientTemp);
	for (int itr = 0; itr < simIters; itr++)
	{
		// initialize sources
		insertSource(dens, temp, ambientTemp, srcRadius, srcHeight, srcDepth, srcVel);	// initialize source densities & temperatures
		addBuoyancyForce(size, w, dens, temp, ambientTemp, dt, gravForce, tempForce);	// add buoyancy forces

		densityStep(&dens_prev, &dens, u, v, w, diff, dt, solverIters);				// compute density update
		temparatureStep(&temp_prev, &temp, u, v, w, heat, dt, solverIters);			// compute temperature update
		velocityStep(&u_prev, &v_prev, &w_prev, &u, &v, &w, visc, dt, solverIters);	// compute velocity update

		swap_ptrs(&dens, &dens_prev);
		swap_ptrs(&temp, &temp_prev);
		swap_ptrs(&u, &u_prev);
		swap_ptrs(&v, &v_prev);
		swap_ptrs(&w, &w_prev);

		vorticityConfinement(size, u_prev, v_prev, w_prev, u, v, w, dt);	// compute vorticity forces

		addVorticityForce(size, u, u_prev, dt);
		addVorticityForce(size, v, v_prev, dt);
		addVorticityForce(size, w, w_prev, dt);

		float pct_complete = (itr + 1) / (float)simIters * 100;
		cout << fixed << setprecision(0);
		cout << "\r" << pct_complete << "% complete" << flush;
	}


	// write density grid to file
	//insertSource(dens, temp, u, v, w, ambientTemp, srcRadius, srcHeight, srcDepth, srcVel);
	writeDensityGrid(dens, bbox0, bbox1);

    

	// free array memory
	free(dens);
	free(dens_prev);
	free(temp);
	free(temp_prev);
	free(u);
	free(u_prev);
	free(v);
	free(v_prev);
	free(w);
	free(w_prev);

}
