
#if defined(_MSC_VER)
#pragma once
#endif

#ifndef PBRT_CAMERAS_REALISTIC_H
#define PBRT_CAMERAS_REALISTIC_H

// cameras/realistic.h*
#include <vector>

#include "pbrt.h"
#include "camera.h"
#include "film.h"

// RealisticCamera Struct Definition

// LensElement: one lens element in the system
typedef struct LensElement {
    float lens_radius;
    float z_pos;
    float index_of_refraction;
    float aperture_radius;
} LensElement;

// ExitPupil: exit pupil represented as the distance of the
// center of the exit pupil from the z-axis and the radius
typedef struct ExitPupil {
    float radius;
    float xy_offset;
} ExitPupil;

// RealisticCamera Declarations
class RealisticCamera : public Camera {
public:
    // RealisticCamera Public Methods
    RealisticCamera(const AnimatedTransform &cam2world, float sopen,
                    float sclose, float filmdistance, float fstop,
                    string specfile, float filmdiag, Film *film,
                    int nExitPupilLensSamples, int nExitPupilFilmSamples,
                    int nBlades);
    float GenerateRay(const CameraSample &sample, Ray *ray) const;

private:

    // RealisticCamera Private Method
    void ParseLensSpecFile(string specfile);

    /**
     * Function: ComputeExitPupilLookupTable
     * -------------------------------------
     * This function attempts to compute the exit pupil sizes for
     * different points on the film plane of varying distances to
     * the z-axis. Essentially we start from every point in a series of
     * sample points on the film plane, from the center to some point
     * filmdiag/2 from the z-axis, and check how far off the ray's
     * direction can be from pointing toward center of the last lens
     * without being blocked in the lens system. These sampled points
     * are all on the y=0 plane and along the x-axis, so the resulting
     * exit pupils are also oriented along the x-axis. This means that
     * given a point some distance from the z-axis on the film plane,
     * we'll be able to know roughly where the exit pupil at the 
     * z-position of the last lens is at. (See writeup for more detailed
     * and diagrammed explanation)
     */
    void ComputeExitPupilLookupTable();

    /**
     * Function: ComputeSampleDiskForLastLens_ExitPupil
     * ------------------------------------------------
     * To generate reasonable sample rays, we need to compute an area 
     * for sampling directions from the film plane to the last lens in
     * the lens system. This function finds the exit pupil from the
     * distance of the pixel on the film plane to the z-axis. 
     *
     * Input:
     *     float pixelDistToZ: the distance from the point on the film
     *                         plane to the z-axis
     *
     * Output:
     *     ExitPupil: the exit pupil to be sampled from
     */
    ExitPupil ComputeSampleDiskForLastLens_ExitPupil(float pixelDistToZ) const;

    /**
     * Function: ComputeSampleDiskForLastLens_Max
     * ------------------------------------------
     * This function computes how large the sample disk at the
     * z-position of the last lens has to be in order to cover all
     * possible directions of rays from any position on the film
     * plane. In the function, we start from the point at the corner
     * of the film plane, shoot a ray from it to the edge of the
     * last lens, and calculate how far the ray is when its z-position
     * is the lens' z-position when it intersects with the z-axis.
     */
    float ComputeSampleDiskForLastLens_Max() const;
    /**
     * Function: ComputeSampleDiskForLastLens_Variable
     * -----------------------------------------------
     * This function computes how large the sample disk at the
     * z-position of the last lens has to be in order to cover all
     * possible directions of rays from one point on the film plane
     * pixelDistToZ from the z-axis. The implementation is very similar
     * to the max function above except that the return value depends on
     * the distance - any distance smaller than the aperture radius of
     * the last lens would return the aperture radius of the last lens,
     * while distances larger would be used to compute the disk radius
     * required to cover all ray directions from that distance.
     */
    float ComputeSampleDiskForLastLens_Variable(float pixelDistToZ) const;

    /**
     * Function: SamplePointOnDisk
     * ---------------------------
     * This function samples a point on the last lens, or a
     * circular disk at the position of the last lens big
     * enough to cover all possible rays toward the last lens.
     *
     * Input:
     *     CameraSample sample: the given CameraSample from GenerateRay
     *     float sampleDiskRadius: the radius of the disk to sample from
     *
     * Output:
     *     Point: a point on the disk sampled from the sample
     */
    Point SamplePointOnDisk(const CameraSample &sample, float sampleDiskRadius) const;
    /**
     * Function: SamplePointOnDisk
     * ---------------------------
     * This function is an overload of the previous function, but this time
     * uses the exit pupil to sample the point on the last lens. Since the
     * exit pupil only contains information of the center of the disk from
     * the z-axis and the radius, we'll need to use the original point on
     * the film plane to rotate the exist pupil from orienting along x-axis
     * to the proper position.
     *
     * Input:
     *     Point origin: the origin of the ray on the film plane, we'll use
     *                   the xy coordinate of this point to rotate the exit pupil
     *                   from x-axis to proper orientation
     *     CameraSample sample: the given CameraSample from GenerateRay
     *     ExitPupil pupil: the exit pupil that corresponds to the points at
     *                      similar distances to the z-axis with the origin
     *
     * Output:
     *     Point: a point on the disk sampled from the sample
     */
    Point SamplePointOnDisk(Point origin, const CameraSample &sample, ExitPupil pupil) const;

    /**
     * Function: TraceThroughLenses
     * ----------------------------
     * This function traces the given ray through the
     * lenses of the camera, calculating refractions
     * along the way and updating the given ray to the
     * ray after passing through all of the lenses.
     *
     * Input:
     *     Ray *ray: Ray to be traced through the lenses,
     *               will be updated if successfully traced
     *
     * Output:
     *     bool: whether the ray passes through the lens
     *           system of the camera
     */
    bool TraceThroughLenses(Ray *ray) const;

    /**
     * Function: IntersectLens
     * -----------------------
     * This function computes whether a given ray would 
     * intersect a given element, returns the t for the
     * ray if it would, and returns -1 if it won't.
     *
     * Input:
     *     const Ray *ray: ray to compute intersection
     *     LensElement elem: the lens element to compute intersection
     *     Normal *n: normal at the intersection if an intersection occurs
     *
     * Output:
     *     float: the value t when the ray intersects with
     *            the lens element. -1 when no intersection
     */
    static float IntersectLens(const Ray *ray, LensElement elem, Normal *n);

    /**
     * Function: Refract
     * -----------------
     * This function computes refraction of a ray through an
     * interface, and returns true if the ray passes through
     * the interface.
     *
     * Input:
     *     Vector v_i: the incident ray direction
     *     Vector v_t: the outgoing ray direction to be filled, if refracted
     *     Normal n: the normal at the intersection point
     *     float n_i, n_t: the refraction indices
     *
     * Output:
     *     bool: true if successfully refracted, false for internal reflection
     */
    static bool Refract(Vector v_i, Vector *v_t, Normal n, float n_i, float n_t);

    /**
     * Function: CheckLensAperture
     * ---------------------------
     * This function checks if a given point is within the
     * aperture radius of a lens element, assuming the point
     * lies on the same z position as the lens.
     *
     * Input:
     *     Point p: the point aperture z-position
     *     float aperture_radius: the aperture radius of the lens element
     *
     * Output:
     *     bool: true if the point is within aperture, false otherwise
     */
    static bool CheckLensAperture(Point p, float aperture_radius);
    bool CheckApertureStop_Regular(Point p, float aperture_radius) const;
    bool CheckApertureStop_Special(Point p, float aperture_radius) const;

    // RealisticCamera Private Data
    float filmdistance;
    float fstop;
    float filmdiag;
    float filmWidth, filmHeight; // film dimensions in mm
    std::vector<LensElement> lenses; // all lenses in the system
    // exit pupils ordered by distance of the film plane point from the z-axis
    std::vector<ExitPupil> exitPupils;

    // these bools determine which sampling method to use
    bool useExitPupil, useVariableDisk, useMaxDisk;
    // variables used when computing exit pupils
    int nExitPupilLensSamples;
    int nExitPupilFilmSamples;

    // variables used for bokeh
    int nBlades; // number of blades

};


RealisticCamera *CreateRealisticCamera(const ParamSet params,
                                       const AnimatedTransform &cam2world, Film *film);

#endif // PBRT_CAMERAS_REALISTIC_H
