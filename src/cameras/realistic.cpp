
// cameras/realistic.cpp*
#include <assert.h>
#include <iostream>

#include "floatfile.h"
#include "stdafx.h"
#include "cameras/realistic.h"
#include "paramset.h"
#include "sampler.h"
#include "montecarlo.h"

// RealisticCamera Method Definitions
RealisticCamera::RealisticCamera(const AnimatedTransform &cam2world, float sopen,
                                 float sclose, float filmdistance, float fstop,
                                 string specfile, float filmdiag, Film *film,
                                 int nExitPupilLensSamples, int nExitPupilFilmSamples,
                                 int nBlades)
    : Camera(cam2world, sopen, sclose, film), filmdistance(filmdistance), fstop(fstop / 2.f),
      filmdiag(filmdiag), nExitPupilLensSamples(nExitPupilLensSamples),
      nExitPupilFilmSamples(nExitPupilFilmSamples),
      nBlades(nBlades) {
    ParseLensSpecFile(specfile);

    float filmAspectRatio = (float) film->yResolution / film->xResolution;
    filmWidth = sqrt(filmdiag * filmdiag / (1.f + filmAspectRatio * filmAspectRatio));
    filmHeight = filmWidth * filmAspectRatio;

    ComputeExitPupilLookupTable();

    useExitPupil = true;
    useVariableDisk = false;
    useMaxDisk = false;
}

void RealisticCamera::ParseLensSpecFile(string specfile) {
    vector<float> specs;
    if (!ReadFloatFile(specfile.c_str(), &specs)) {
       Severe( "Reading %s unsuccessful.\n", specfile.c_str() );
    }


    assert (specs.size() % 4 == 0);

    float z_pos = 0.f;
    for (unsigned int i = 0; i < specs.size(); i += 4) {
        LensElement elem;
        elem.lens_radius = specs.at(i);
        elem.z_pos = z_pos;
        elem.index_of_refraction = specs.at(i+2);
        if (elem.lens_radius == 0.f) {
            elem.aperture_radius = fstop;
        } else {
            elem.aperture_radius = specs.at(i+3) / 2.f;
        }
        lenses.push_back(elem);
        z_pos -= specs.at(i+1);
    }
}

void RealisticCamera::ComputeExitPupilLookupTable() {
    const float filmStepSize = filmdiag / 2.f / nExitPupilFilmSamples;
    const float maxRadius = ComputeSampleDiskForLastLens_Max();
    const float sampleStepSize = maxRadius / nExitPupilLensSamples;

    const float lensPos = lenses.at(lenses.size() - 1).z_pos;
    const float filmPos = lensPos - filmdistance;

    for (int fBin = 0; fBin < nExitPupilFilmSamples; ++fBin) {
        float exitPupilMaxDist = - maxRadius;
        float exitPupilMinDist = maxRadius;
        for (int sample = 0; sample < nExitPupilLensSamples; ++sample) {
            Point filmP(fBin * filmStepSize, 0.f, filmPos);
            Point lensPosP(sample * sampleStepSize, 0.f, lensPos);
            Ray rPos(filmP, Normalize(lensPosP - filmP), 0.f);
            if (TraceThroughLenses(&rPos)) {
                if (exitPupilMaxDist < lensPosP.x) {
                    exitPupilMaxDist = lensPosP.x;
                }
                if (exitPupilMinDist > lensPosP.x) {
                    exitPupilMinDist = lensPosP.x;
                }
            }
            Point lensNegP( - sample * sampleStepSize, 0.f, lensPos);
            Ray rNeg(filmP, Normalize(lensNegP - filmP), 0.f);
            if (TraceThroughLenses(&rNeg)) {
                if (exitPupilMinDist > lensNegP.x) {
                    exitPupilMinDist = lensNegP.x;
                }
                if (exitPupilMinDist > lensNegP.x) {
                    exitPupilMinDist = lensNegP.x;
                }
            }
        }
        exitPupilMaxDist += sampleStepSize;
        exitPupilMinDist -= sampleStepSize;
        ExitPupil pupil;
        pupil.xy_offset = (exitPupilMaxDist + exitPupilMinDist) / 2.f;
        pupil.radius = (exitPupilMaxDist - exitPupilMinDist) / 2.f;
        exitPupils.push_back(pupil);
    }
}

ExitPupil RealisticCamera::ComputeSampleDiskForLastLens_ExitPupil(float pixelDistToZ) const {
    unsigned int bin = exitPupils.size() * pixelDistToZ / (filmdiag / 2.f);
    if (bin >= exitPupils.size()) bin = exitPupils.size() - 1;
    if (bin < 0) bin = 0;
    return exitPupils.at(bin);
}

float RealisticCamera::ComputeSampleDiskForLastLens_Max() const {
    return ComputeSampleDiskForLastLens_Variable(filmdiag / 2.f);
}

float RealisticCamera::ComputeSampleDiskForLastLens_Variable(float pixelDistToZ) const {
    LensElement lastLens = lenses.at(lenses.size() - 1);
    if (pixelDistToZ < lastLens.aperture_radius) {
        return lastLens.aperture_radius;
    }
    // first, find the distance from the lens' specified z-position (where it
    // intersects with the z-axis) to the z-position of the center of the lens'
    // disk (z-position where the lens' edge ends)
    float center_to_lens = fabs(lastLens.lens_radius);
    if (center_to_lens > lastLens.aperture_radius) {
        center_to_lens -= sqrt(pow(lastLens.lens_radius, 2) - pow(lastLens.aperture_radius, 2));
    }
    // then find the intersection of z-axis and the line formed by connecting the
    // pixel on the film and the edge of the lens
    float center_to_vert = lastLens.aperture_radius * (center_to_lens + filmdistance) /
                           (pixelDistToZ - lastLens.aperture_radius);
    // by similarity the disk should be the aperture radius scaled up by the added
    // distance from the center of the lens disk to the lens' z-position
    return lastLens.aperture_radius * (1.f + center_to_lens/center_to_vert);
}

float RealisticCamera::GenerateRay(const CameraSample &sample, Ray *ray) const {
    // compute position on the film plane
    Point origin( 0.f, 0.f, 0.f );
    origin.x = - filmWidth * (sample.imageX / film->xResolution - 0.5f);
    origin.y = filmHeight * (sample.imageY / film->yResolution - 0.5f);
    origin.z = lenses.at(lenses.size() - 1).z_pos - filmdistance;
    // sample from origin position to the last lens
    // first compute how far the pixel (origin point of ray) is from the z-axis
    float pixelDistToZ = sqrt(pow(origin.x, 2) + pow(origin.y, 2));
    // this distance is used to determine how large the disk we'll be sampling from
    // should be with the ComputeSampleDiskForLastLens function(s)
    ExitPupil exitPupil;
    float sampleDiskRadius = ComputeSampleDiskForLastLens_Max();
    Point sampleLastLensPoint;
    if (useExitPupil) {
        exitPupil = ComputeSampleDiskForLastLens_ExitPupil(pixelDistToZ);
        sampleLastLensPoint = SamplePointOnDisk(origin, sample, exitPupil);
    } else if (useVariableDisk) {
        sampleDiskRadius = ComputeSampleDiskForLastLens_Variable(pixelDistToZ);
        sampleLastLensPoint = SamplePointOnDisk(sample, sampleDiskRadius);
    } else if (useMaxDisk) {
        sampleLastLensPoint = SamplePointOnDisk(sample, sampleDiskRadius);
    }
    // compute the direction from the origin point to the sampled point and form a ray
    Vector dir = Normalize(sampleLastLensPoint - origin);
    float cos_theta = Dot(dir, Vector(0.f, 0.f, 1.f));
    Ray cameraSpaceRay = Ray(origin, dir, 0.f);
    // trace through lenses to check if the ray will go through
    if (!TraceThroughLenses(&cameraSpaceRay)) {
        // no weight if the ray won't pass the lens system
        return 0.f;
    }
    // return correct ray o/d after tracing through in world coordinate
    CameraToWorld(cameraSpaceRay, ray);
    ray->d = Normalize(ray->d);
    // return weighting (cosine^4 * Area / dist^2)
    if (useExitPupil) {
        return pow(cos_theta, 4) * M_PI * pow(exitPupil.radius, 2) / pow(filmdistance, 2);
    }
    return pow(cos_theta, 4) * M_PI * pow(sampleDiskRadius, 2) / pow(filmdistance, 2);
}

Point RealisticCamera::SamplePointOnDisk(const CameraSample &sample, float sampleDiskRadius) const {
    float lensX, lensY;
    ConcentricSampleDisk(sample.lensU, sample.lensV, &lensX, &lensY);
    lensX *= sampleDiskRadius;
    lensY *= sampleDiskRadius;
    // the z-position would be on the lens' specified position
    return Point(lensX, lensY, lenses.at(lenses.size() - 1).z_pos);
}

Point RealisticCamera::SamplePointOnDisk(Point origin, const CameraSample &sample, ExitPupil pupil) const {
    float lensX, lensY;
    ConcentricSampleDisk(sample.lensU, sample.lensV, &lensX, &lensY);
    Vector rotateDir(origin.x, origin.y, 0.f);
    rotateDir = Normalize(rotateDir);
    Point sampleP = Point(0.f, 0.f, lenses.at(lenses.size() - 1).z_pos) + rotateDir * pupil.xy_offset;
    sampleP.x += lensX * rotateDir.x * pupil.radius;
    sampleP.y += lensY * rotateDir.y * pupil.radius;
    // the z-position would be on the lens' specified position
    return sampleP;
}

bool RealisticCamera::TraceThroughLenses(Ray *ray) const {
    float n_i = 1.f, n_t = 1.f;

    for (int i = lenses.size() - 1; i >= 0; --i) {
        Normal intersect_n;
        LensElement lens = lenses.at(i);

        // figure out the index of refraction for the two media from the spec file
        n_i = lens.index_of_refraction;
        if (i == 0) {
            n_t = 1.f;
        } else {
            n_t = lenses.at(i - 1).index_of_refraction;
            if (n_t == 0.f) n_t = 1.f; // the aperture stop's N is 0 in spec file
        }

        // if the element is an aperture stop
        if (lens.lens_radius == 0.f) {
            float t = (lens.z_pos - ray->o.z) / ray->d.z;
            if (t < 0) return false;
            if (!CheckApertureStop_Regular((*ray)(t), lens.aperture_radius)) {
                return false;
            }
            ray->o = (*ray)(t);
        } else {
            // if the element is a lens, check intersection with the lens first
            float t = IntersectLens(ray, lens, &intersect_n);
            if (t < 0) return false;
            if (!CheckLensAperture((*ray)(t), lens.aperture_radius)) {
                return false;
            }
            ray->o = (*ray)(t);
            // refract the ray on the lens and check for total internal reflection
            if (!Refract(ray->d, &(ray->d), intersect_n, n_i, n_t)) {
                return false;
            }
        }
    }
    // finish iterating through all elements, ray passed
    return true;
}

float RealisticCamera::IntersectLens(const Ray *ray, LensElement elem, Normal *n) {
    // solve for (t * d + o - p) ^2 = r^2, where d is the ray direction,
    // o the ray origin, p the center of the spherical lens, and r the radius
    // or equivalently, d^2 * t^2 + 2 * d * (o-p) * t + (o-p)^2 - r^2 = 0
    Point lens_center = Point(0.f, 0.f, elem.z_pos - elem.lens_radius);
    float a = ray->d.LengthSquared();
    float b = 2 * Dot(ray->d, ray->o - lens_center);
    float c = (Vector(ray->o) - Vector(lens_center)).LengthSquared()
              - elem.lens_radius * elem.lens_radius;
    float t_small, t_large;

    if (Quadratic(a, b, c, &t_small, &t_large)) {
        if (elem.lens_radius > 0) {
            Normal norm((*ray)(t_large) - lens_center);
            *n = Normalize( - norm );
            return t_large;
        } else {
            Normal norm((*ray)(t_small) - lens_center);
            *n = Normalize(norm);
            return t_small;
        }
    }
    return -1.f;
}

bool RealisticCamera::Refract(Vector v_i, Vector *v_t, Normal n, float n_i, float n_t) {
    v_i = Normalize(v_i);
    n = Normalize(n);
    float cos_i = - Dot(n, v_i);
    float sin_i = sqrt(1 - cos_i * cos_i);
    float sin_t = n_i * sin_i / n_t;
    if (sin_t > 1.f) return false;

    // separate the outgoing firection into two components,
    // parallel to N and perpendicular to N
    float cos_t = sqrt(1 - sin_t * sin_t);
    Vector t_parallel = Vector(- n * cos_t);
    Vector t_perpendicular = sin_t * Normalize(v_i + Vector(cos_i * n));
    *v_t = Normalize(t_parallel + t_perpendicular);
    return true;
}

bool RealisticCamera::CheckApertureStop_Regular(Point p, float aperture_radius) const {
    float distToZ = sqrt(p.x * p.x + p.y * p.y);
    float phi = acos(p.x / distToZ);
    float theta = M_PI / nBlades;
    for (int i = 0; i < nBlades; ++i) {
        float deg = 2 * M_PI / nBlades * (i + 0.5);
        float diff = fabs(deg - phi);
        if (diff < theta) {
            theta = diff;
        }
    }
    return distToZ * cos(theta) < aperture_radius * cos(M_PI / nBlades);
}

bool RealisticCamera::CheckApertureStop_Special(Point p, float aperture_radius) const {
    float distToZ = sqrt(p.x * p.x + p.y * p.y);
    float cos_theta = p.x / distToZ;
    float sin_theta = p.y / distToZ;
    float r  = fstop * (2 - 2 * sin_theta + sin_theta * sqrt(fabs(cos_theta)) / (sin_theta + 1.4));
    return distToZ < r;
}

bool RealisticCamera::CheckLensAperture(Point p, float aperture_radius) {
    return (p.x * p.x + p.y * p.y) < aperture_radius * aperture_radius;
}


RealisticCamera *CreateRealisticCamera(const ParamSet params,
                                       const AnimatedTransform &cam2world,
                                       Film *film) {
    // Extract common camera parameters from \use{ParamSet}
    float shutteropen = params.FindOneFloat("shutteropen", -1);
    float shutterclose = params.FindOneFloat("shutterclose", -1);

    // Realistic camera-specific parameters
    string specfile = params.FindOneString("specfile", "");
    float filmdistance = params.FindOneFloat("filmdistance", 70.0); // about 70 mm default to film
    float fstop = params.FindOneFloat("aperture_diameter", 1.0);
    float filmdiag = params.FindOneFloat("filmdiag", 35.0);
    int nExitPupilLensSamples = params.FindOneInt("exit_pupil_lens_samples", 512);
    int nExitPupilFilmSamples = params.FindOneInt("exit_pupil_film_samples", 512);
    int nBlades = params.FindOneInt("blades", 6);
    assert(shutteropen != -1 && shutterclose != -1 && filmdistance!= -1);
    if (specfile == "") {
        Severe( "No lens spec file supplied!\n" );
    }
    return new RealisticCamera(cam2world, shutteropen, shutterclose, filmdistance,
                               fstop, specfile, filmdiag, film, nExitPupilLensSamples,
                               nExitPupilFilmSamples, nBlades);
}