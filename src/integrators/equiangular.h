
#if defined(_MSC_VER)
#pragma once
#endif

#ifndef PBRT_INTEGRATORS_EQUIANGULAR_H
#define PBRT_INTEGRATORS_EQUIANGULAR_H

// integrators/equiangular.h*
#include "volume.h"
#include "integrator.h"

// EquiAngularIntegrator Declarations
class EquiAngularIntegrator : public VolumeIntegrator {
public:
    // EquiAngularIntegrator Public Methods
    EquiAngularIntegrator(float ass, float ss) {
        angularStepSize = ass;
        stepSize = ss;
		Assert(stepSize > 0.f);
    }
    Spectrum Transmittance(const Scene *, const Renderer *,
        const RayDifferential &ray, const Sample *sample, RNG &rng,
        MemoryArena &arena) const;
    void RequestSamples(Sampler *sampler, Sample *sample,
        const Scene *scene);
    Spectrum Li(const Scene *, const Renderer *, const RayDifferential &ray,
         const Sample *sample, RNG &rng, Spectrum *T, MemoryArena &arena) const;
private:
    // EquiAngularIntegrator Private Data
    float angularStepSize;
    int angleSampleOffset, lightSampleOffset;
    float stepSize;
    int tauSampleOffset, scatterSampleOffset;
};


EquiAngularIntegrator *CreateEquiAngularIntegrator(const ParamSet &params);

#endif // PBRT_INTEGRATORS_EQUIANGULAR_H
