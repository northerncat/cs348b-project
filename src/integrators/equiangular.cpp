
// integrators/equiangular.cpp*
#include "stdafx.h"
#include "integrators/equiangular.h"
#include "scene.h"
#include "paramset.h"
#include "montecarlo.h"

// EquiAngularIntegrator Method Definitions
void EquiAngularIntegrator::RequestSamples(Sampler *sampler, Sample *sample,
        const Scene *scene) {
    angleSampleOffset = sample->Add1D(1);
    tauSampleOffset = sample->Add1D(1);
    scatterSampleOffset = sample->Add1D(1);
}


Spectrum EquiAngularIntegrator::Transmittance(const Scene *scene,
        const Renderer *renderer, const RayDifferential &ray,
        const Sample *sample, RNG &rng, MemoryArena &arena) const {
    if (!scene->volumeRegion) return Spectrum(1.f);
    float step, offset;
    if (sample) {
        step = stepSize;
        offset = sample->oneD[tauSampleOffset][0];
    }
    else {
        step = 4.f * stepSize;
        offset = rng.RandomFloat();
    }
    Spectrum tau = scene->volumeRegion->tau(ray, step, offset);
    return Exp(-tau);
}

void printSpectrum(Spectrum& s, string name) {
    RGBSpectrum test = s.ToRGBSpectrum();
    float rgb[3];
    test.ToRGB(rgb);
    std::cout << name << "=(" << rgb[0] << "," << rgb[1] << "," << rgb[2] << ")" << std::endl;
}


Spectrum EquiAngularIntegrator::Li(const Scene *scene, const Renderer *renderer,
        const RayDifferential &ray, const Sample *sample, RNG &rng,
        Spectrum *T, MemoryArena &arena) const {
    VolumeRegion *vr = scene->volumeRegion;
    Ray r(ray.o, Normalize(ray.d), ray.mint, ray.maxt, ray.time, ray.depth);
    float t0, t1;
    if (!vr || !vr->IntersectP(r, &t0, &t1) || (t1-t0) == 0.f) {
        *T = 1.f;
        return 0.f;
    }
    // Do single scattering volume integration in _vr_
    Spectrum totalLv(0.);

    int nLights = scene->lights.size();
    float *lightRNG = arena.Alloc<float>(3 * nLights);
    for (int i = 0; i < 3 * nLights; ++i) {
        lightRNG[i] = rng.RandomFloat();
    }

    float t0Copy = t0;
    float t1Copy = t1;

    for (int ln = 0; ln < nLights; ++ln) {
        // int ln = min(nLights - 1, Floor2Int(rng.RandomFloat * nLights));

        // set up sampling a point on the light source
        Light* light = scene->lights[ln];
        LightSample ls(lightRNG[3 * ln], lightRNG[3 * ln + 1], lightRNG[3 * ln + 2]);

        t0 = t0Copy;
        t1 = t1Copy;
        Point start = r(t0), end = r(t1);

        Point lightP;
        Normal n;

        if (light->Sample_Point(start, ls, lightP, &n)) {
            // compute the angles and distance to the light source point
            float D = r.distanceTo(lightP);
            Point projP = r.closestPointTo(lightP);
            float projT = (projP.x - r.o.x) / r.d.x;
            float thetaA = acosf(Dot(Normalize(start - lightP), Normalize(projP - lightP)));
            if (projT > t0) {
                thetaA = thetaA < 0 ? thetaA : -thetaA;
            } else if (thetaA != thetaA) {
                *T = 1.f;
                return 0.f;
            }
            float thetaB = acosf(Dot(Normalize(end - lightP), Normalize(projP - lightP)));
            if (projT > t1) {
                thetaB = thetaB < 0 ? thetaB : -thetaB;
            } else if (thetaB != thetaB) {
                *T = 1.f;
                return 0.f;
            }

            // setup for the ray marching iterations
            int nAngularSamples = Ceil2Int((thetaB - thetaA) / angularStepSize);
            if (nAngularSamples < 0) {
                *T = 1.f;
                return 0.f;
            }
            float thetaRange = thetaB - thetaA;
            float angleStep = (thetaB - thetaA) / nAngularSamples;
            float step = (t1 - t0) / nAngularSamples;

            Spectrum Tr(1.f);
            Point pAngular = r(t0), pPrevAngular;
            float t = t0 - projT, tPrev;
            Vector w = -r.d;
            float angle = thetaA + angleStep * sample->oneD[angleSampleOffset][0];

            // Spectrum TrStep(1.f);
            // Point p = ray(t0), pPrev;
            // t0 += sample->oneD[scatterSampleOffset][0] * step;

            // float *lightComp = arena.Alloc<float>(nAngularSamples);
            // LDShuffleScrambled1D(1, nAngularSamples, lightComp, rng);
            // float *lightPos = arena.Alloc<float>(2*nAngularSamples);
            // LDShuffleScrambled2D(1, nAngularSamples, lightPos, rng);
            // uint32_t sampOffset = 0;

            Spectrum angularLv(0.);
            Spectrum emissiveLv(0.);
            Spectrum stepLv(0.);
            for (int i = 0; i < nAngularSamples && angle < M_PI / 2.f; ++i, angle += angleStep, t0 += step) {
                // Advance to sample at _t0_ and update _T_
				if (i == 0) {
					tPrev = D * tanf(thetaA);
					t = D * tanf(angle);
				}
				else {
					tPrev = t;
					t = D * tanf(angle);
				}
                float pdf = D / (thetaRange * (pow(D, 2.f) + pow(t, 2.f)));

                pPrevAngular = pAngular;
                pAngular = r(projT + t);
                Ray tauRay(pPrevAngular, pAngular - pPrevAngular, 0.f, 1.f, r.time, r.depth);
                Spectrum stepTau = vr->tau(tauRay,
                                           .5f * stepSize, rng.RandomFloat());
                Tr *= Exp(-stepTau);

                // pPrev = p;
                // p = ray(t0);
                // Ray stepTauRay(pPrev, p - pPrev, 0.f, 1.f, r.time, r.depth);
                // stepTau = vr->tau(tauRay, .5f * step, rng.RandomFloat());
                // TrStep *= Exp(-stepTau);

                // Possibly terminate ray marching if transmittance is small
                if (Tr.y() < 1e-3) {
                    const float continueProb = .5f;
                    if (rng.RandomFloat() > continueProb) {
                        Tr = 0.f;
                        break;
                    }
                    Tr /= continueProb;
                }

                emissiveLv += Tr * vr->Lve(pAngular, w, r.time);
                // Compute single-scattering source term at _p_
                Spectrum ss = vr->sigma_s(pAngular, w, r.time);
                if (!ss.IsBlack()) {
                    // compute the radiance from the pre-sampled light source
                    float lightPdf = 1.f;
                    VisibilityTester vis;
                    Vector lightToP = Normalize(pAngular - lightP);
                    Spectrum angularL = light->L_From(lightP, pAngular, n, lightToP, &lightPdf, &vis);

                    // single scattering contribution by sampling a random light source
                    // float samplePdf;
                    // VisibilityTester sampleVis;
                    // Vector wo;
                    // LightSample stepLightSample(lightComp[sampOffset], lightPos[2*sampOffset],
                    //                             lightPos[2*sampOffset+1]);
                    // Spectrum sampledL = light->Sample_L(p, 0.f, stepLightSample, ray.time, &wo, &samplePdf, &sampleVis);

                    // combine two contributions with MIS
                    // printSpectrum(angularL, "angularHere");
                    // std::cout << lightPdf << std::endl;
                    if (!angularL.IsBlack() && pdf > 0.f && lightPdf > 0.f && vis.Unoccluded(scene)) {
                        Ray shadowRay(pAngular, -lightToP, 0.f, (lightP - pAngular).Length(), r.time, r.depth);
                        Spectrum shadowTau = vr->tau(shadowRay, stepSize, rng.RandomFloat());

                        Spectrum scatter = Tr * ss * vr->p(pAngular, w, -lightToP, r.time) * Exp(- shadowTau);
                        // printSpectrum(scatter, "scatter1");
                        scatter *= angularL;
                        // printSpectrum(scatter, "scatter2");
                        scatter *= vis.Transmittance(scene, renderer, NULL, rng, arena) / (pdf * lightPdf);
                        // printSpectrum(scatter, "scatter3");
                        float weight = 1.f;
                        // if (samplePdf > 0.f) {
                        //     weight = PowerHeuristic(1, lightPdf * pdf, 1, samplePdf);
                        // }
                        angularLv += scatter * weight;
                    }

                    // if (!sampledL.IsBlack() && samplePdf > 0.f && sampleVis.Unoccluded(scene)) {
                    //     Spectrum Ld = sampledL * sampleVis.Transmittance(scene, renderer, NULL, rng, arena);
                    //     Spectrum scatter = Tr * ss * vr->p(p, w, -wo, r.time) * Ld / (samplePdf * pdf);
                    //     float weight = 1.f;
                    //     if (pdf * lightPdf > 0.f) {
                    //         weight = PowerHeuristic(1, samplePdf * pdf, 1, lightPdf * pdf);
                    //     }
                    //     stepLv += scatter * weight;
                    // }
                }
                // ++sampOffset;
            }
            *T = Tr;
            totalLv += (emissiveLv / float(nLights) + angularLv + stepLv) * angleStep;
        }
    }
    return totalLv;
}


EquiAngularIntegrator *CreateEquiAngularIntegrator(const ParamSet &params) {
    float ass  = params.FindOneFloat("angularstepsize", 0.1f);
    float ss  = params.FindOneFloat("stepsize", 0.1f);
    return new EquiAngularIntegrator(ass, ss);
}


