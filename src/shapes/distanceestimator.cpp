
// shape/distanceestimator.cpp
#include "shapes/distanceestimator.h"
#include "paramset.h"


// DistanceEstimator Method Definitions
DistanceEstimator::DistanceEstimator(const Transform *o2w, const Transform *w2o, bool ro, 
                                     DistanceEstimatorParams params)
    : Shape(o2w, w2o, ro), params(params) {}

bool DistanceEstimator::Intersect(const Ray &r, float *tHit, float *rayEpsilon,
                       DifferentialGeometry *dg) const {

    Ray ray;
    (*WorldToObject)(r, &ray);

    int iter = 0;
    float thit = ray.mint;
    Vector dir = Normalize(ray.d);
    Point p = ray.o + thit * dir;
    float distance = Evaluate(p);

    while (iter < params.maxIters && thit < ray.maxt && distance > params.hitEpsilon) {
        p += distance * dir;
        thit += distance;
        ++iter;
        distance = Evaluate(p);
    }

    if (distance > params.hitEpsilon)
        return false;

    *tHit = thit;
    *rayEpsilon = params.rayEpsilonMultiplier * params.hitEpsilon;

    Vector normal = CalculateNormal(p, params.normalEpsilon);
    Vector dpdu, dpdv;
    CoordinateSystem(normal, &dpdu, &dpdv);
    Normal dndu(0,0,0), dndv(0,0,0);

    const Transform &o2w = *ObjectToWorld;
    *dg = DifferentialGeometry(o2w(p), o2w(dpdu), o2w(dpdv), o2w(dndu),
                               o2w(dndv), 0.0, 0.0, this);

    return true;
}


bool DistanceEstimator::IntersectP(const Ray &r) const {

    Ray ray;
    (*WorldToObject)(r, &ray);

    int iter = 0;
    float thit = ray.mint;
    Vector dir = Normalize(ray.d);
    Point p = ray.o + thit * dir;
    float distance = Evaluate(p);

    while (iter < params.maxIters && thit < ray.maxt && distance > params.hitEpsilon) {
        p += distance * dir;
        thit += distance;
        distance = Evaluate(p);
        ++iter;
    }

    return distance < params.hitEpsilon;
}

Vector DistanceEstimator::CalculateNormal(const Point& pos, float eps) const {
    const Vector v1 = Vector( 1.0,-1.0,-1.0);
    const Vector v2 = Vector(-1.0,-1.0, 1.0);
    const Vector v3 = Vector(-1.0, 1.0,-1.0);
    const Vector v4 = Vector( 1.0, 1.0, 1.0);

    return Normalize( 
            v1 * Evaluate( pos + v1*eps ) +
            v2 * Evaluate( pos + v2*eps ) +
            v3 * Evaluate( pos + v3*eps ) +
            v4 * Evaluate( pos + v4*eps ) );
}