

#ifndef PBRT_SHAPES_MANDELBULBDE_H
#define PBRT_SHAPES_MANDELBULBDE_H

// shapes/mandelbulbde.h
#include "shapes/distanceestimator.h"

class MandelbulbDE : public DistanceEstimator {
public:
    // MandelbulbDE public methods
    MandelbulbDE(const Transform *o2w, const Transform *w2o, bool ro,
                 DistanceEstimatorParams params, int fractalIters, int mandelbulbPower);
    BBox ObjectBound() const;
    float Area() const;

    float Evaluate(const Point& p) const;

private:
    // MandelbulbDE Private Data
    int fractalIters;
    int mandelbulbPower;
};

MandelbulbDE *CreateMandelbulbDEShape(const Transform *o2w, const Transform *w2o,
                                      bool reverseOrientation, ParamSet paramSet);


#endif // PBRT_SHAPES_MANDELBULBDE_H
