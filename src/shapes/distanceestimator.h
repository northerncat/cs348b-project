


#ifndef PBRT_SHAPES_DISTANCEESTIMATOR_H
#define PBRT_SHAPES_DISTANCEESTIMATOR_H

// shapes/distanceestimator.h
#include "shape.h"

typedef struct {
    int maxIters; // Number of steps along the ray until we give up (default 1000)
    float hitEpsilon; // how close to the surface we must be before we say we "hit" it 
    float rayEpsilonMultiplier; // how much we multiply hitEpsilon by to get rayEpsilon 
    float normalEpsilon; // The epsilon we send to CalculateNormal()
} DistanceEstimatorParams;

// DistanceEstimator class declarations
class DistanceEstimator : public Shape {
public:
    // DistanceEstimator public methods
    DistanceEstimator(const Transform *o2w, const Transform *w2o, bool ro, 
                      DistanceEstimatorParams params);
    virtual BBox ObjectBound() const = 0;
    virtual float Area() const = 0;
    bool Intersect(const Ray &ray, float *tHit, float *rayEpsilon,
                   DifferentialGeometry *dg) const;
    bool IntersectP(const Ray &ray) const;

    virtual float Evaluate(const Point& p) const = 0;

private:
    // DistanceEstimator private methods
    Vector CalculateNormal(const Point& pos, float eps) const;
    // DistanceEstimator Private Data
    float radius;
protected:
    // DistanceEstimator Protected Data
    DistanceEstimatorParams params;
};

#endif // PBRT_SHAPES_DISTANCEESTIMATOR_H
