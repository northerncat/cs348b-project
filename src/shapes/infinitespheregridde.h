

#ifndef PBRT_SHAPES_INFINITESPHEREGRIDDE_H
#define PBRT_SHAPES_INFINITESPHEREGRIDDE_H

// shapes/infinitespheregridde.h
#include "shapes/distanceestimator.h"

class InfiniteSphereGridDE : public DistanceEstimator {
public:
    // InfiniteSphereGridDE public methods
    InfiniteSphereGridDE(const Transform *o2w, const Transform *w2o, bool ro,
                         DistanceEstimatorParams params, float cellSize, float radius);
    BBox ObjectBound() const;
    float Area() const;

    float Evaluate(const Point& p) const;

private:
    // InfiniteSphereGridDE Private Data
    float cellSize;
    float radius;
};

InfiniteSphereGridDE *CreateInfiniteSphereGridDEShape(const Transform *o2w,
                                                      const Transform *w2o,
                                                      bool reverseOrientation,
                                                      ParamSet paramSet);


#endif // PBRT_SHAPES_INFINITESPHEREGRIDDE_H
