

#ifndef PBRT_SHAPES_TORUSGRIDDE_H
#define PBRT_SHAPES_TORUSGRIDDE_H

// shapes/torusgridde.h
#include "shapes/distanceestimator.h"

class TorusGridDE : public DistanceEstimator {
public:
    // TorusGridDE public methods
    TorusGridDE(const Transform *o2w, const Transform *w2o, bool ro,
                      DistanceEstimatorParams params, float cellSize, float R, float r);
    BBox ObjectBound() const;
    float Area() const;

    float Evaluate(const Point& p) const;

private:
    // TorusGridDE private methods
    float EvaluateTorus(float x, float y, float z) const;
    // TorusGridDE Private Data
    float cellSize;
    float majorRadius;
    float minorRadius;
};

TorusGridDE *CreateTorusGridDEShape(const Transform *o2w, const Transform *w2o,
                                    bool reverseOrientation, ParamSet paramSet);


#endif // PBRT_SHAPES_TORUSGRIDDE_H
