

#ifndef PBRT_SHAPES_SPHEREDE_H
#define PBRT_SHAPES_SPHEREDE_H

// shapes/spherede.h
#include "shapes/distanceestimator.h"

class SphereDE : public DistanceEstimator {
public:
    // SphereDE public methods
    SphereDE(const Transform *o2w, const Transform *w2o, bool ro, 
             DistanceEstimatorParams params, float radius);
    BBox ObjectBound() const;
    float Area() const;

    float Evaluate(const Point& p) const;

private:
    // SphereDE Private Data
    float radius;
};

SphereDE *CreateSphereDEShape(const Transform *o2w, const Transform *w2o,
                              bool reverseOrientation, ParamSet paramSet);


#endif // PBRT_SHAPES_SPHEREDE_H
