
// shape/infinitespheregridde.cpp
#include "shapes/infinitespheregridde.h"
#include "paramset.h"

#include <limits>
#include <math.h>

// InfiniteSphereGridDE Method Definitions
InfiniteSphereGridDE::InfiniteSphereGridDE(const Transform *o2w, const Transform *w2o,
                                           bool ro, DistanceEstimatorParams params,
                                           float cellSize, float radius)
    : DistanceEstimator(o2w, w2o, ro, params), cellSize(cellSize), radius(radius) {}

BBox InfiniteSphereGridDE::ObjectBound() const {
    return BBox(Point(-1000000, -1000000, -1000000),
                Point( 1000000,  1000000,  1000000));
}

float InfiniteSphereGridDE::Area() const {
    return std::numeric_limits<float>::infinity();
}

float InfiniteSphereGridDE::Evaluate(const Point& p) const {
    float dx = remainder(p.x, cellSize);
    float dy = remainder(p.y, cellSize);
    float dz = remainder(p.z, cellSize);
    return sqrtf(dx*dx + dy*dy + dz*dz) - radius;
}

InfiniteSphereGridDE *CreateInfiniteSphereGridDEShape(const Transform *o2w,
                                                      const Transform *w2o,
                                                      bool reverseOrientation,
                                                      ParamSet paramSet) {

    DistanceEstimatorParams params;
    params.maxIters = paramSet.FindOneInt("maxiters", 100);
    params.hitEpsilon = paramSet.FindOneFloat("hitEpsilon", 5e-4f);
    params.rayEpsilonMultiplier = paramSet.FindOneFloat("rayEpsilonMultiplier", 100.f);
    params.normalEpsilon = paramSet.FindOneFloat("normalEpsilon", 5e-5f);

    float cellSize = paramSet.FindOneFloat("cellSize", 2.f);
    float radius = paramSet.FindOneFloat("radius", 1.f);

    return new InfiniteSphereGridDE(o2w, w2o, reverseOrientation, params, cellSize, radius);
}