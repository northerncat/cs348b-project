
// shape/spherede.cpp
#include "shapes/spherede.h"
#include "paramset.h"


// SphereDE Method Definitions
SphereDE::SphereDE(const Transform *o2w, const Transform *w2o, bool ro, 
                   DistanceEstimatorParams params, float radius)
    : DistanceEstimator(o2w, w2o, ro, params), radius(radius) {}

BBox SphereDE::ObjectBound() const {
    return BBox(Point(-radius, -radius, -radius),
                Point( radius,  radius, radius));
}

float SphereDE::Area() const {
    return 4 * M_PI * radius * radius;
}

float SphereDE::Evaluate(const Point& p) const {
    return sqrtf(p.x*p.x + p.y*p.y + p.z*p.z) - radius;
}

SphereDE *CreateSphereDEShape(const Transform *o2w, const Transform *w2o,
                              bool reverseOrientation, ParamSet paramSet) {

    float radius = paramSet.FindOneFloat("radius", 1.f);

    DistanceEstimatorParams params;
    params.maxIters = paramSet.FindOneInt("maxIters", 100);
    params.hitEpsilon = paramSet.FindOneFloat("hitEpsilon", 5e-4f);
    params.rayEpsilonMultiplier = paramSet.FindOneFloat("rayEpsilonMultiplier", 50.f);
    params.normalEpsilon = paramSet.FindOneFloat("normalEpsilon", 5e-5f);

    return new SphereDE(o2w, w2o, reverseOrientation, params, radius);
}