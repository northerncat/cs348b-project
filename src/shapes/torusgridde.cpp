
// shape/torusgridde.cpp
#include "shapes/torusgridde.h"
#include "paramset.h"

#include <limits>
#include <math.h>
#include <stdlib.h>

// TorusGridDE Method Definitions
TorusGridDE::TorusGridDE(const Transform *o2w, const Transform *w2o, bool ro,
                         DistanceEstimatorParams params, float cellSize, float R, float r)
    : DistanceEstimator(o2w, w2o, ro, params),
      cellSize(cellSize), majorRadius(R), minorRadius(r) {}

BBox TorusGridDE::ObjectBound() const {
    return BBox(Point(-1000000, -1000000, -1000000),
                Point( 1000000,  1000000,  0));
}

float TorusGridDE::Area() const {
    return std::numeric_limits<float>::infinity();
}

float TorusGridDE::Evaluate(const Point& p) const {
    if (p.z > majorRadius + minorRadius) {
        return p.z - majorRadius - minorRadius + params.hitEpsilon * 2;
    }
    float offsets[3];
    offsets[0] = remainder(p.x, cellSize);
    offsets[1] = remainder(p.y, cellSize);
    offsets[2] = remainder(p.z, cellSize);
    float dist = min(EvaluateTorus(offsets[0], offsets[1], offsets[2]),
                     EvaluateTorus(offsets[1], offsets[2], offsets[0]));
    dist = min(dist, EvaluateTorus(offsets[2], offsets[0], offsets[1]));
    return dist;
}

float TorusGridDE::EvaluateTorus(float x, float y, float z) const {
    float projectDist = sqrtf(x*x + y*y) - majorRadius;
    return sqrtf(projectDist*projectDist + z*z) - minorRadius;
}

TorusGridDE *CreateTorusGridDEShape(const Transform *o2w, const Transform *w2o,
                                    bool reverseOrientation, ParamSet paramSet) {

    DistanceEstimatorParams params;
    params.maxIters = paramSet.FindOneInt("maxiters", 100);
    params.hitEpsilon = paramSet.FindOneFloat("hitEpsilon", 5e-4f);
    params.rayEpsilonMultiplier = paramSet.FindOneFloat("rayEpsilonMultiplier", 100.f);
    params.normalEpsilon = paramSet.FindOneFloat("normalEpsilon", 5e-5f);

    float cellSize = paramSet.FindOneFloat("cellSize", 2.f);
    float R = paramSet.FindOneFloat("majorRadius", 1.f);
    float r = paramSet.FindOneFloat("minorRadius", 0.1f);

    return new TorusGridDE(o2w, w2o, reverseOrientation, params, cellSize, R, r);
}