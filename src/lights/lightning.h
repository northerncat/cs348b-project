
#if defined(_MSC_VER)
#pragma once
#endif

#ifndef PBRT_LIGHTS_LIGHTNING_H
#define PBRT_LIGHTS_LIGHTNING_H

// lights/Lightning.h*
#include "core/octree.h"
#include "pbrt.h"
#include "light.h"
#include "primitive.h"
#include "rng.h"

struct LightningOctreeNode;

struct LightningLine {
    LightningLine() { }
    LightningLine(const Point &f, const Point& t, float I) : from(f), to(t), intensity(I) { }

    Point from;
    Point to;
    float intensity;
};

// Lightning Declarations
class Lightning : public Light {
public:
    // Lightning Public Methods
    Lightning(const Spectrum &emit, const BBox &e, const Transform &l2w,
            int x, int y, int z, const float *d)
        : Light(l2w), Lemit(emit), octreeBounds(e) {
        GenerateLightning(d, x, y, z);
    }

    ~Lightning();

    Spectrum Power(const Scene *) const;
    bool IsDeltaLight() const { return false; }
    bool Sample_Point(const Point& p, const LightSample& ls, Point& sample, Normal *n) const;
    Spectrum L_From(const Point& sampleP, const Point& p, const Normal& n,
                    const Vector& w, float* pdf, VisibilityTester *vis) const;
    float Pdf(const Point &, const Vector &) const;
    Spectrum Sample_L(const Point &P, float pEpsilon, const LightSample &ls, float time,
        Vector *wo, float *pdf, VisibilityTester *visibility) const;
    Spectrum Sample_L(const Scene *scene, const LightSample &ls, float u1, float u2,
        float time, Ray *ray, Normal *Ns, float *pdf) const;

protected:
    // Lightning Protected Data
    void GenerateLightning(const float *d, int x, int y, int z);

    Spectrum Lemit;

    BBox octreeBounds;
    LightningOctreeNode *octree;
    MemoryArena octreeArena;
    RNG rng;
};


Lightning *CreateLightning(const Transform &light2world, const ParamSet &paramSet);

#endif // PBRT_LIGHTS_LIGHTNING_H
