
// lights/lightning.cpp*
#include "stdafx.h"
#include "lights/lightning.h"
#include "paramset.h"
#include "montecarlo.h"


void printSpectrum(const Spectrum& s, string name) {
    RGBSpectrum test = s.ToRGBSpectrum();
    float rgb[3];
    test.ToRGB(rgb);
    std::cout << name << "=(" << rgb[0] << "," << rgb[1] << "," << rgb[2] << ")" << std::endl;
}

void printPoint(const Point& p, string name) {
    std::cout << name << "= (" << p.x << "," << p.y << "," << p.z << ")" << std::endl;
}

float Length(const LightningLine& l) {
    return (l.from - l.to).Length();
}

void printLightningLine(const LightningLine &l) {
    printPoint(l.from, "from");
    printPoint(l.to, "to");
    std::cout << "Intensity=" << l.intensity << std::endl;
    std::cout << "Length=" << Length(l) << std::endl;

}

struct LightningOctreeNode {
    // LightningOctreeNode Methods
    LightningOctreeNode() {
        isLeaf = true;
        sumIntensity = 0.f;
        for (int i = 0; i < 8; ++i)
            lps[i] = NULL;
    }
    void Insert(const BBox &nodeBound, LightningLine *lp, MemoryArena &arena) {
        Point pMid = .5f * nodeBound.pMin + .5f * nodeBound.pMax;
        if (isLeaf) {
            // Add _LightningPoint_ to leaf octree node
            for (int i = 0; i < 8; ++i) {
                if (!lps[i]) {
                    lps[i] = lp;
                    return;
                }
            }

            // Convert leaf node to interior node, redistribute points
            isLeaf = false;
            LightningLine *localIps[8];
            for (int i = 0; i < 8; ++i) {
                localIps[i] = lps[i];
                children[i] = NULL;
            }
            for (int i = 0; i < 8; ++i)  {
                LightningLine *lp = localIps[i];
                // Add _LightningPoint_ _ip_ to interior octree node
                int child = (lp->from.x > pMid.x ? 4 : 0) +
                    (lp->from.y > pMid.y ? 2 : 0) + (lp->from.z > pMid.z ? 1 : 0);
                if (!children[child])
                    children[child] = arena.Alloc<LightningOctreeNode>();
                BBox childBound = octreeChildBound(child, nodeBound, pMid);
                children[child]->Insert(childBound, lp, arena);
            }
            /* fall through to interior case to insert the new point... */
        }
        // Add _LightningPoint_ _lp_ to interior octree node
        int child = (lp->from.x > pMid.x ? 4 : 0) +
            (lp->from.y > pMid.y ? 2 : 0) + (lp->from.z > pMid.z ? 1 : 0);
        if (!children[child])
            children[child] = arena.Alloc<LightningOctreeNode>();
        BBox childBound = octreeChildBound(child, nodeBound, pMid);
        children[child]->Insert(childBound, lp, arena);
    }
    void InitHierarchy() {
        if (isLeaf) {
            // Init _LightningOctreeNode_ leaf from _LightningPoint_s
            sumIntensity = 0.f;
            uint32_t i;
            for (i = 0; i < 8; ++i) {
                if (!lps[i]) break;
                float wt = lps[i]->intensity * Length(*lps[i]);
                // printLightningLine(*lps[i]);
                p += wt * lps[i]->from;
                sumIntensity += wt;
            }
            if (sumIntensity > 0.f) p /= sumIntensity;
        }
        else {
            // Init interior _LightningOctreeNode_
            sumIntensity = 0.f;
            uint32_t nChildren = 0;
            for (uint32_t i = 0; i < 8; ++i) {
                if (!children[i]) continue;
                ++nChildren;
                children[i]->InitHierarchy();
                float wt = children[i]->sumIntensity;
                p += wt * children[i]->p;
                sumIntensity += wt;
            }
            if (sumIntensity > 0.f) p /= sumIntensity;
        }
    }
    void SampleLine(const RNG& rng, LightningLine& line, float* pdf) {
        if (isLeaf) {
            float intensities[8];
            int i;
            for (i = 0; i < 8; ++i) {
                if (!lps[i]) break;
                intensities[i] = lps[i]->intensity * Length(*lps[i]);
            }
            Distribution1D dist(&intensities[0], i);
            float u = rng.RandomFloat();
            float segmentPdf;
            dist.SampleContinuous(u, &segmentPdf, &i);
            if (!lps[i]) {
                *pdf = 0.f;
                return;
            }

            *pdf *= segmentPdf;
            line = *lps[i];
        } else {
            float intensities[8];
            int i;
            for (i = 0; i < 8; ++i) {
                if (!children[i]) break;
                intensities[i] = children[i]->sumIntensity;
            }
            Distribution1D dist(&intensities[0], i);
            float u = rng.RandomFloat();
            float segmentPdf;
            dist.SampleContinuous(u, &segmentPdf, &i);
            if (!children[i]) {
                *pdf = 0.f;
                return;
            }

            *pdf *= segmentPdf;
            children[i]->SampleLine(rng, line, pdf);
        }
    }
    LightningLine* Lookup(const BBox nodeBound, const Point& p, float epsilon, float* pdf) {
        // printPoint(p, "looking up p ");
        if (!nodeBound.Inside(p)) {
            // std::cout << "out of bound" << std::endl;
            *pdf = 0.f;
            return NULL;
        }
        if (isLeaf) {
            for (int i = 0; i < 8; ++i) {
                if (!lps[i]) continue;
                // printLightningLine(*lps[i]);
				if ((lps[i]->from - p).Length() == 0.f || (lps[i]->to - p).Length() == 0.f) {
					// std::cout << "pre leaf pdf=" << *pdf;
					*pdf *= Length(*lps[i]) * lps[i]->intensity / sumIntensity;
					// std::cout << "post leaf pdf=" << *pdf;
					return lps[i];
				}
                if ((Normalize(lps[i]->from - p) - Normalize(p - lps[i]->to)).Length() < epsilon) {
                    // std::cout << "pre leaf pdf=" << *pdf;
                    *pdf *= Length(*lps[i]) * lps[i]->intensity / sumIntensity;
                    // std::cout << "post leaf pdf=" << *pdf;
                    return lps[i];
                }
            }
            // std::cout << "can't find line in leaf" << std::endl;
            *pdf = 0.f;
            return NULL;
        }
        Point pMid = nodeBound.pMin * 0.5 + nodeBound.pMax * 0.5;
        int child = (p.x > pMid.x ? 4 : 0) + (p.y > pMid.y ? 2 : 0) + (p.z > pMid.z ? 1 : 0);
        if (!children[child])
            // std::cout << "no children" << std::endl;
            *pdf = 0.f;
            return NULL;
        BBox childBound = octreeChildBound(child, nodeBound, pMid);
        // std::cout << "pre int pdf=" << *pdf;
        *pdf *= children[child]->sumIntensity / sumIntensity;
        // std::cout << "post int pdf=" << *pdf;
        return children[child]->Lookup(childBound, p, epsilon, pdf);
    }

    // LightningOctreeNode Public Data
    Point p;
    bool isLeaf;
    float sumIntensity;
    union {
        LightningOctreeNode *children[8];
        LightningLine *lps[8];
    };
};

// Lightning Method Definitions
Lightning::~Lightning() {
    octreeArena.FreeAll();
}

Spectrum Lightning::Power(const Scene *) const {
    return Lemit * octree->sumIntensity;
}


bool Lightning::Sample_Point(const Point& p, const LightSample& ls, Point& sample, Normal *n) const {
 
    float pdf = 1.f;
    LightningLine l;
    octree->SampleLine(rng, l, &pdf);
    pdf *= Length(l);

    if (pdf == 0.f) return false;

    sample = l.from * ls.uPos[0] + l.to * (1.f - ls.uPos[0]);
    return true;
}

Spectrum Lightning::L_From(const Point& sampleP, const Point& p, const Normal& n,
                                  const Vector& w, float* pdf, VisibilityTester *vis) const {

    *pdf = 1.f;
    LightningLine* lp = octree->Lookup(octreeBounds, sampleP, 1e-2f, pdf);
    if (!lp) {
        *pdf = 0.f;
        return Spectrum(0.f);
    }

    *pdf /= Length(*lp);

    vis->SetSegment(p, 1e-3f, sampleP, 1e-3f, 0.f);
    // printSpectrum(Lemit, "Lemit");
    Spectrum Ls = lp->intensity * Lemit;
    return Ls / DistanceSquared(sampleP, p);
}


Spectrum Lightning::Sample_L(const Point &p, float pEpsilon,
        const LightSample &ls, float time, Vector *wi, float *pdf,
        VisibilityTester *visibility) const {

    *pdf = 1.f;
    LightningLine l;
    octree->SampleLine(rng, l, pdf);

    if (*pdf == 0.f) return Spectrum(0.f);

    *pdf /= Length(l);
    Point ps = l.from * ls.uPos[0] + l.to * (1.f - ls.uPos[0]);
    *wi = Normalize(ps - p);

    visibility->SetSegment(p, pEpsilon, ps, 1e-3f, time);

    Spectrum Ls = l.intensity * Lemit;

    return Ls / DistanceSquared(ps, p);
}


float Lightning::Pdf(const Point &p, const Vector &wi) const {
    float pdf = 1.f;
    LightningLine* lp = octree->Lookup(octreeBounds, p, 1e-3f, &pdf);
    if (!lp) return 0.f;
    return pdf / Length(*lp);
}


Spectrum Lightning::Sample_L(const Scene *scene,
        const LightSample &ls, float u1, float u2, float time,
        Ray *ray, Normal *Ns, float *pdf) const {

    *pdf = 1.f;
    LightningLine l;
    octree->SampleLine(rng, l, pdf);

    *pdf /= Length(l);
    if (*pdf == 0.f) return Spectrum(0.f);

    Point org = l.from * ls.uPos[0] + l.to * (1.f - ls.uPos[0]);

    Vector dir = UniformSampleSphere(u1, u2);
    *ray = Ray(org, dir, 1e-3f, INFINITY, time);

    Spectrum Ls = l.intensity * Lemit;

    return Ls;
}

float D(const float *d, int i, int j, int k, int nx, int ny, int nz) {
    Assert(i >= 0 && i < nx && j >= 0 && j < ny && k >= 0 && k < nz);
    return d[k * nx * ny + j * nx + i];
}

Point LerpPoint(const Point& p1, const Point& p2, float x, float y, float z) {
    Point p;
    p.x = p1.x * x + p2.x * (1.f - x);
    p.y = p1.y * y + p2.y * (1.f - y);
    p.z = p1.z * z + p2.z * (1.f - z);;
    return p;
}

void Lightning::GenerateLightning(const float *d, int nx, int ny, int nz) {
    // Create octree of clustered irradiance samples
    octree = octreeArena.Alloc<LightningOctreeNode>();

    // TODO: DBM
    Point from (octreeBounds.pMin);
    Point to (octreeBounds.pMax);

    LightningLine *l = octreeArena.Alloc<LightningLine>();
    l->from = .9 * from + 0.1 * to;
    l->to = 0.9 * from + 0.1 * to;
    l->to.y = to.y;
    l->intensity = 1000.f;
    octree->Insert(octreeBounds, l, octreeArena);
    
    l = octreeArena.Alloc<LightningLine>();
    l->from = LerpPoint(from, to, 0.0, 0.0, 0.9);
    l->to = LerpPoint(from, to, 0.9, 0.0, 0.9);
    l->intensity = 1000.f;
    octree->Insert(octreeBounds, l, octreeArena);
    
    l = octreeArena.Alloc<LightningLine>();
    l->from = LerpPoint(from, to, 1.0, 0.0, 0.0);
    l->to = LerpPoint(from, to, 0.0, 1.0, 1.0);
    l->intensity = 1000.f;
    octree->Insert(octreeBounds, l, octreeArena);

    octree->InitHierarchy();
}


Lightning *CreateLightning(const Transform &light2world, const ParamSet &paramSet) {
    // Initialize common volume region parameters
    Spectrum Le = paramSet.FindOneSpectrum("Le", 0.);
    Point p0 = paramSet.FindOnePoint("p0", Point(0,0,0));
    Point p1 = paramSet.FindOnePoint("p1", Point(1,1,1));
    int nitems;
    const float *data = paramSet.FindFloat("density", &nitems);
    if (!data) {
        Error("No \"density\" values provided for volume grid?");
        return NULL;
    }
    int nx = paramSet.FindOneInt("nx", 1);
    int ny = paramSet.FindOneInt("ny", 1);
    int nz = paramSet.FindOneInt("nz", 1);
    if (nitems != nx*ny*nz) {
        Error("Lightning has %d density values but nx*ny*nz = %d",
            nitems, nx*ny*nz);
        return NULL;
    }
    return new Lightning(Le, BBox(p0, p1), light2world, nx, ny, nz, data);
}


